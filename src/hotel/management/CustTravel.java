package hotel.management;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
public class CustTravel extends javax.swing.JInternalFrame {
    public CustTravel() {
        initComponents();
        com1();
        com2();
        Icon.Icon("/Icon/Travel_Bus.png", this);

    }

    public final void com1() {
        try {
            Connection con = con1.connection();
            JComboBox comboBox1 = new javax.swing.JComboBox();
            Statement smt = con.createStatement();
            String Qry1 = "Select Tour.Package From Tour;";
            ResultSet rs = smt.executeQuery(Qry1);
            while (rs.next()) {
                comboBox1.addItem(rs.getString("Package"));
            }
            tab.getColumn("Tour Package").setCellEditor(new DefaultCellEditor(comboBox1));
            
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public final void com2() {
        try {
            Connection con = con1.connection();
            JComboBox comboBox2 = new javax.swing.JComboBox();
            Statement smt = con.createStatement();
            String Qry2 = "Select Transport.Mode From Transport;";
            ResultSet rt = smt.executeQuery(Qry2);

            while (rt.next()) {
                comboBox2.addItem(rt.getString("Mode"));

            }
            this.tab.getColumn("Transport").setCellEditor(new DefaultCellEditor(comboBox2));
            
        } catch (ClassNotFoundException | SQLException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                                "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    public float Price = 0.0f;
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        CNoT = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        PriceT = new javax.swing.JTextField();

        setTitle("Customer Travel");

        jLabel1.setText("Cust_No");

        tab.setAutoCreateRowSorter(true);
        tab.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Tour Package", "Transport", "Km", "Fare/Km", "Total"
            }
        ));
        tab.setCellSelectionEnabled(true);
        tab.setRowHeight(25);
        tab.setRowMargin(3);
        tab.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                tabAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        tab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tab);
        tab.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        jButton3.setText("Generate Price");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton1.setText("OK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cancel");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel3.setText("Total Price");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(PriceT, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addGap(42, 42, 42)
                                .addComponent(CNoT, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(78, 78, 78)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 726, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(CNoT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PriceT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tabAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_tabAncestorAdded
        
    }//GEN-LAST:event_tabAncestorAdded

    private void tabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabKeyPressed
      
    }//GEN-LAST:event_tabKeyPressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
    try {
        Connection con= con1.connection();
        int j=tab.getRowCount();
        System.out.println(j);
        String Qry1="Select * FROM Tour;";
        String Qr2="Select * From Transport;";
        float rate=0.0f;
        float km=0.0f;
        float Total=0.0f;
        Statement smt= con.createStatement();
        for(int i=0;i<j;i++)
        {
            Object o1=tab.getModel().getValueAt(i, 0);
            Object o2=tab.getModel().getValueAt(i, 1);
            System.out.println(o1+""+o2);
            if(o1==null&&o2==null)
            {
                break;
            }
            else
            {
                ResultSet R1,R2;
                R1=smt.executeQuery(Qry1);
                while(R1.next())
                {
                    String pkg= R1.getString("Package");
                    if(pkg.equals(o1))
                    {
                        String Km=R1.getString("Km");
                        tab.getModel().setValueAt(Km, i, 2);
                        System.out.println("Km set");
                        km=Float.parseFloat(tab.getModel().getValueAt(i,2).toString());
                        System.out.println(km);                        
                    }
                 }
                R2=smt.executeQuery(Qr2);
                while(R2.next())
                {
                    String tra=R2.getString("Mode");
                    if(tra.equals(o2))
                    {
                        String Rate=R2.getString("PPK");
                        tab.getModel().setValueAt(Rate, i, 3);
                        System.out.println("Rate set");
                        rate=Float.parseFloat(tab.getModel().getValueAt(i,3).toString());
                        System.out.println(rate);
                    }
                }
                Total= Total+(km*rate);      
                tab.getModel().setValueAt((km*rate), i, 4);
            }
            Price=Price+Total;
            System.out.print("Price:"+Price);
            PriceT.setText(Price+"");
        }
                 

        } catch (ClassNotFoundException | SQLException | NumberFormatException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                                "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            Connection con = con1.connection();
            String Cno;

            Cno = CNoT.getText();
            
            
           
            String Qry2 = "SELECT cust.Price From cust WHERE Cust_No='" + Cno + "';";

            String PT = PriceT.getText().toString().trim();
            Statement smt = con.createStatement();
            ResultSet rs = smt.executeQuery(Qry2);
            String Pri = null;
            while (rs.next()) {
                Pri = rs.getString("Price");
                System.out.println(Pri);
            }
            Price = Float.parseFloat(Pri) + Float.parseFloat(PT);
            System.out.println(Price);
            String Qry = "UPDATE cust "
                    + "SET cust.Price= '" + Price + ""
                    + "'WHERE cust.Cust_No='" + Cno + "';";
            int cont = smt.executeUpdate(Qry);
            System.out.print(cont);
            if (cont >= 1) {
                JOptionPane.showMessageDialog(null, "Request Success!!!\n Total Payable Money= " + PriceT.getText() + " /-");
                
                this.dispose();

            } else {
                JOptionPane.showMessageDialog(null, "Request Not Complete !!!", "Error", JOptionPane.ERROR_MESSAGE);
            }
            
        } catch (ClassNotFoundException | SQLException | NumberFormatException | HeadlessException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                                "Error", JOptionPane.ERROR_MESSAGE);
        }
        catch(NullPointerException ex)
        {
            JOptionPane.showMessageDialog(null, "Required field(s) Empty !!!", 
                        "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField CNoT;
    private javax.swing.JTextField PriceT;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable tab;
    // End of variables declaration//GEN-END:variables
}
