package hotel.management;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

public class TRans extends javax.swing.JInternalFrame {

    public TRans() throws ClassNotFoundException, SQLException {
        initComponents();
        UP();
        Icon.Icon("/Icon/car_taxi.png", this);
    }

    public final void UP() throws ClassNotFoundException, SQLException {
        Connection Con = con1.connection();
        String Qry = "Select * From Transport";
        Statement smt = Con.createStatement();
        ResultSet rs = smt.executeQuery(Qry);
        table.setModel(DbUtils.resultSetToTableModel(rs));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        Add = new javax.swing.JRadioButton();
        Update = new javax.swing.JRadioButton();
        Delete = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        OT = new javax.swing.JTextField();
        PT = new javax.swing.JTextField();
        ADDB = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        ONR = new javax.swing.JRadioButton();
        PR = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        NN = new javax.swing.JTextField();
        ON = new javax.swing.JTextField();
        UPB1 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        N = new javax.swing.JTextField();
        NP = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        UB2 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        DELB = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        ND = new javax.swing.JTextField();
        DELAB = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        setTitle("Set Transportation");

        buttonGroup1.add(Add);
        Add.setText("Add");
        Add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddActionPerformed(evt);
            }
        });

        buttonGroup1.add(Update);
        Update.setText("Update");
        Update.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                UpdateMouseClicked(evt);
            }
        });
        Update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdateActionPerformed(evt);
            }
        });

        buttonGroup1.add(Delete);
        Delete.setText("Delete");
        Delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeleteActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setEnabled(false);

        jLabel1.setText("Transport");

        jLabel2.setText("Fare/Km");

        ADDB.setText("Add");
        ADDB.setEnabled(false);
        ADDB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ADDBActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(OT)
                    .addComponent(PT))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(150, 150, 150)
                .addComponent(ADDB, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(OT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(PT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(ADDB)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setEnabled(false);

        buttonGroup2.add(ONR);
        ONR.setText("Transport Name");
        ONR.setEnabled(false);
        ONR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ONRActionPerformed(evt);
            }
        });

        buttonGroup2.add(PR);
        PR.setText("Fare");
        PR.setEnabled(false);
        PR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PRActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setEnabled(false);

        jLabel3.setText("New Name");

        jLabel4.setText("Old Name");

        UPB1.setText("Update");
        UPB1.setEnabled(false);
        UPB1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UPB1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ON, javax.swing.GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                    .addComponent(NN, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(140, 140, 140)
                .addComponent(UPB1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(ON, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(NN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(UPB1)
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.setEnabled(false);

        jLabel5.setText("Package");

        jLabel6.setText("New Fare");

        UB2.setText("Update");
        UB2.setEnabled(false);
        UB2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UB2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(NP)
                    .addComponent(N))
                .addContainerGap())
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(139, 139, 139)
                .addComponent(UB2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(NP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(UB2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ONR)
                            .addComponent(PR, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ONR)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PR)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setEnabled(false);

        DELB.setText("Delete");
        DELB.setEnabled(false);
        DELB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DELBActionPerformed(evt);
            }
        });

        jLabel7.setText("Transport");

        DELAB.setText("Delete All");
        DELAB.setEnabled(false);
        DELAB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DELABActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(ND)
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addComponent(DELB)
                .addGap(18, 18, 18)
                .addComponent(DELAB)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(ND, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DELB)
                    .addComponent(DELAB))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButton1.setText("Done");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        table.setAutoCreateRowSorter(true);
        table.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "T1", "T2"
            }
        ));
        table.setEnabled(false);
        jScrollPane1.setViewportView(table);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(2, 2, 2)
                            .addComponent(Add))
                        .addComponent(Update)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Delete)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Add)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Update)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Delete)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(6, 6, 6))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddActionPerformed

        ADDB.setEnabled(true);
        DELB.setEnabled(false);
        DELAB.setEnabled(false);
        UB2.setEnabled(false);
        UPB1.setEnabled(false);
        ONR.setEnabled(false);
        PR.setEnabled(false);
    }//GEN-LAST:event_AddActionPerformed

    private void UpdateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_UpdateMouseClicked
    }//GEN-LAST:event_UpdateMouseClicked

    private void UpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UpdateActionPerformed
        ADDB.setEnabled(false);
        DELB.setEnabled(false);
        ONR.setEnabled(true);
        PR.setEnabled(true);
        UB2.setEnabled(false);
        DELAB.setEnabled(false);
        UPB1.setEnabled(false);
    }//GEN-LAST:event_UpdateActionPerformed

    private void DeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeleteActionPerformed
        DELAB.setEnabled(true);
        ADDB.setEnabled(false);
        DELB.setEnabled(true);
        ONR.setEnabled(false);
        PR.setEnabled(false);
        UB2.setEnabled(false);
        UPB1.setEnabled(false);
    }//GEN-LAST:event_DeleteActionPerformed

    private void ADDBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ADDBActionPerformed
        try {
            String order, Price;
            order = OT.getText().trim();
            Price = PT.getText().trim();
            Connection Con = con1.connection();
            String Qry = "INSERT INTO Transport"
                    + " VALUES('" + order + "','" + Price + "');";
            Statement smt = Con.createStatement();
            int cont = smt.executeUpdate(Qry);
            System.out.print(cont);
            if (cont >= 1) {
                JOptionPane.showMessageDialog(null, "Request Success!!!");
                OT.setText(null);
                PT.setText(null);
            } else {
                JOptionPane.showMessageDialog(null, "Request Not Complete !!!",
                        "Error", JOptionPane.ERROR_MESSAGE);
                OT.setText(null);
                PT.setText(null);
            }
            UP();
            
        } catch (ClassNotFoundException | SQLException | HeadlessException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                                "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_ADDBActionPerformed

    private void ONRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ONRActionPerformed
        DELAB.setEnabled(false);
        ADDB.setEnabled(false);
        DELB.setEnabled(false);
        UB2.setEnabled(false);
        UPB1.setEnabled(true);
    }//GEN-LAST:event_ONRActionPerformed

    private void PRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PRActionPerformed
        DELAB.setEnabled(false);
        ADDB.setEnabled(false);
        DELB.setEnabled(false);
        UB2.setEnabled(true);
        UPB1.setEnabled(false);
    }//GEN-LAST:event_PRActionPerformed

    private void UPB1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UPB1ActionPerformed
        try {
            String OLD, NEW;
            OLD = ON.getText().trim();
            NEW = NN.getText().trim();
            Connection Con = con1.connection();
            String Qry = "UPDATE Transport "
                    + "SET Transport.Mode='" + NEW + "'"
                    + "WHERE Transport.Mode='" + OLD + "';";
            Statement smt = Con.createStatement();
            int cont = smt.executeUpdate(Qry);
            System.out.print(cont);
            if (cont >= 1) {
                JOptionPane.showMessageDialog(null, "Request Success!!!");
                ON.setText(null);
                NN.setText(null);
            } else {
                JOptionPane.showMessageDialog(null, "Request Not Complete !!!",
                        "Error", JOptionPane.ERROR_MESSAGE);
                ON.setText(null);
                NN.setText(null);
            }
            UP();
            
        } catch (ClassNotFoundException | SQLException | HeadlessException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                                "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_UPB1ActionPerformed

    private void UB2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UB2ActionPerformed
        try {
            String Name, Price;
            Name = N.getText().trim();
            Price = NP.getText().trim();
            Connection Con = con1.connection();
            String Qry = "UPDATE Transport "
                    + "SET Transport.PPK='" + Price + "'"
                    + "WHERE Transport.Mode='" + Name + "';";
            Statement smt = Con.createStatement();
            int cont = smt.executeUpdate(Qry);
            System.out.print(cont);
            if (cont >= 1) {
                JOptionPane.showMessageDialog(null, "Request Success!!!");
                N.setText(null);
                NP.setText(null);
            } else {
                JOptionPane.showMessageDialog(null, "Request Not Complete !!!",
                        "Error", JOptionPane.ERROR_MESSAGE);
                N.setText(null);
                NP.setText(null);
            }
            UP();
            
        } catch (ClassNotFoundException | SQLException | HeadlessException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                                "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_UB2ActionPerformed

    private void DELBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DELBActionPerformed
        try {
            String Name;
            Name = ND.getText().trim();

            Connection Con = con1.connection();
            String Qry = "DELETE * FROM Transport WHERE Transport.Mode='" + Name + "';";

            Statement smt = Con.createStatement();
            int cont = smt.executeUpdate(Qry);
            System.out.print(cont);
            if (cont >= 1) {
                JOptionPane.showMessageDialog(null, "Request Success!!!");
                ND.setText(null);

            } else {
                JOptionPane.showMessageDialog(null, "No Field Available!!! ",
                        "Error", JOptionPane.ERROR_MESSAGE);
                ND.setText(null);
            }
            UP();
            
        } catch (ClassNotFoundException | SQLException | HeadlessException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                                "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_DELBActionPerformed

    private void DELABActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DELABActionPerformed
        int n = JOptionPane.showConfirmDialog(
                null,
                "Delete All???",
                "Delete",
                JOptionPane.YES_NO_OPTION);
        if (n == JOptionPane.YES_OPTION) {
            try {

                Connection Con = con1.connection();
                String Qry = "Delete tour FROM Transport WHERE Transport.Mode"
                        + " <>'None';";


                Statement smt = Con.createStatement();
                int cont = smt.executeUpdate(Qry);
                System.out.print(cont);
                if (cont >= 1) {
                    JOptionPane.showMessageDialog(null, "Request Success!!!");

                } else {
                    JOptionPane.showMessageDialog(null, "No Field Available!!! " + "!!!",
                            "Error", JOptionPane.ERROR_MESSAGE);
                }
                UP();
                
            } catch (ClassNotFoundException | SQLException | HeadlessException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                                "Error", JOptionPane.ERROR_MESSAGE);
        }
        } else {
            //
        }
    }//GEN-LAST:event_DELABActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ADDB;
    private javax.swing.JRadioButton Add;
    private javax.swing.JButton DELAB;
    private javax.swing.JButton DELB;
    private javax.swing.JRadioButton Delete;
    private javax.swing.JTextField N;
    private javax.swing.JTextField ND;
    private javax.swing.JTextField NN;
    private javax.swing.JTextField NP;
    private javax.swing.JTextField ON;
    private javax.swing.JRadioButton ONR;
    private javax.swing.JTextField OT;
    private javax.swing.JRadioButton PR;
    private javax.swing.JTextField PT;
    private javax.swing.JButton UB2;
    private javax.swing.JButton UPB1;
    private javax.swing.JRadioButton Update;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
}
