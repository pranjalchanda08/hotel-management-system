package hotel.management;

import java.sql.*;
import java.text.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class CustOut extends javax.swing.JInternalFrame {

    public float dd(String dateStart, String dateStop) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        Date d1;
        Date d2;
        d1 = format.parse(dateStart);
        d2 = format.parse(dateStop);
        float diff = d2.getTime() - d1.getTime();
        float diffDays = diff / (24 * 60 * 60 * 1000);
        if (diffDays == 0) {
            diffDays = 1;
        }
        System.out.print(diffDays + " days, ");
        return diffDays;
    }

  
    public CustOut() {
        initComponents();
        Icon.Icon("/Icon/Cust min.png", this);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        OK = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        IDT = new javax.swing.JTextField();
        NameT = new javax.swing.JTextField();
        RNoT = new javax.swing.JTextField();

        setTitle("Customer Out");

        jLabel1.setText("Customer No");

        jLabel2.setText("Name");

        jLabel3.setText("RoomNo");

        OK.setText("OK");
        OK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OKActionPerformed(evt);
            }
        });
        OK.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                OKKeyPressed(evt);
            }
        });

        jButton2.setText("Cancel");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(OK, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(jButton2))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(RNoT)
                            .addComponent(IDT, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(NameT, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(IDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(NameT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(RNoT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 21, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(OK)
                    .addComponent(jButton2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void OKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OKActionPerformed
        try {
            Connection con = con1.connection();
            Statement smt = con.createStatement();
            String InDate = null;
            Date date = new Date();
            String UID = IDT.getText(), Rno = RNoT.getText();
            String Qry5 = "Select ODate From cust Where cust.Cust_No='" + UID + "';";
            ResultSet R = smt.executeQuery(Qry5);
            
           
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String Cur_Date = sdf.format(date);
                    String p = null;
                    String Qry4 = "SELECT A.Price FROM room A ,cust B "
                            + "WHERE A.Type=B.Type AND B.Cust_No='" + UID + "';";
                    ResultSet r3 = smt.executeQuery(Qry4);
                    while (r3.next()) {
                        p = r3.getString("Price");
                    }
                    System.out.print(p);
                    String Qry3 = "SELECT In_Date From cust WHERE cust.Cust_No='" + UID
                            + "';";
                    ResultSet r = smt.executeQuery(Qry3);
                    while (r.next()) {
                        InDate = r.getString("In_Date");
                    }
                    System.out.println(Cur_Date);
                    System.out.println(InDate);
                    float dif = this.dd(InDate, Cur_Date);
                    String Pc = null;
                    String Qry2 = "SELECT Price From cust WHERE cust.Cust_No='" + UID + "';";
                    ResultSet rt = smt.executeQuery(Qry2);
                    while (rt.next()) {
                        Pc = rt.getString("Price");
                    }
                    float Pay = (Float.parseFloat(Pc) + (Float.parseFloat(p) * dif));
                    String Qry = "UPDATE cust "
                            + "SET cust.ODate='" + Cur_Date + "',cust.Price='" + Pay
                            + "'WHERE Cust_No='" + UID + "'and RoomNo ='" + Rno + "';";
                    int cont = smt.executeUpdate(Qry);
                    System.out.print(cont);
                    if (cont >= 1) {
                        JOptionPane.showMessageDialog(null, "Request Success!!!\n Total Payable Money= " + Pay + " /-");
                        this.dispose();

                    } else {
                        JOptionPane.showMessageDialog(null, "Request Not Complete !!!", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                                            
        }
        catch (ClassNotFoundException | SQLException | ParseException ex)
        {
           Logger.getLogger(CustOut.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch(NullPointerException ex)
        {
            JOptionPane.showMessageDialog(null, "Required field(s) Empty !!!", 
                        "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_OKActionPerformed

    private void OKKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_OKKeyPressed
    }//GEN-LAST:event_OKKeyPressed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField IDT;
    private javax.swing.JTextField NameT;
    private javax.swing.JButton OK;
    private javax.swing.JTextField RNoT;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    // End of variables declaration//GEN-END:variables
}
