package hotel.management;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class Cust_Order extends javax.swing.JInternalFrame {

    public Cust_Order() {
        initComponents();
        this.getcombo();
        Icon.Icon("/Icon/1373694581_Person_CoffeeBreak_Male_Dark.png", this);
    }
 public final void getcombo()
 {
  try {
            
           
            Connection con = con1.connection();
            Statement smt = con.createStatement();
            String Qry3 = "Select Order From Items;";
            ResultSet rs = smt.executeQuery(Qry3);
            JComboBox comboBox = new javax.swing.JComboBox();
            
            while (rs.next()) 
            {
                comboBox.addItem(rs.getString("Order"));

            }
            jTable1.getColumn("Order").setCellEditor(new DefaultCellEditor
                    (comboBox));
            
            
        } catch (ClassNotFoundException | SQLException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                                "Error", JOptionPane.ERROR_MESSAGE);
        }
 }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        CNoT = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        PriceT = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();

        setTitle("Customer Order");

        jLabel1.setText("Cust_No");

        jLabel3.setText("Total Price");

        jButton1.setText("OK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cancel");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTable1.setAutoCreateRowSorter(true);
        jTable1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Order", "Qty", "Price", "Total"
            }
        ));
        jTable1.setCellSelectionEnabled(true);
        jTable1.setRowHeight(25);
        jTable1.setRowMargin(3);
        jTable1.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jTable1AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        jButton3.setText("Generate Price");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(PriceT, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addGap(42, 42, 42)
                                .addComponent(CNoT, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(78, 78, 78)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 854, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(CNoT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 517, Short.MAX_VALUE)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(PriceT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public float Price = 0.0f;
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            Connection con = con1.connection();
            String Cno;
            Cno = CNoT.getText();
            
            
            String Qry2 = "SELECT cust.Price From cust WHERE Cust_No='" 
                    + Cno + "';";
            String PT = PriceT.getText().toString().trim();
            Statement smt = con.createStatement();
            ResultSet rs = smt.executeQuery(Qry2);
            String Pri = null;
            while (rs.next()) {
                Pri = rs.getString("Price");
                System.out.println(Pri);
            }
            Price = Float.parseFloat(Pri) + Float.parseFloat(PT);
            System.out.println(Price);
            String Qry = "UPDATE cust "
                    + "SET cust.Price= '" + Price + ""
                    + "'WHERE cust.Cust_No='" + Cno + "';";
            int cont = smt.executeUpdate(Qry);
            System.out.print(cont);
            if (cont >= 1) {
                JOptionPane.showMessageDialog(null, "Request Success!!!\n Total"
                        + " Payable Money= " + PriceT.getText() + " /-");
                
                this.dispose();

            } else {
                JOptionPane.showMessageDialog(null, "Request Not Complete !!!", 
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
           
        } catch (ClassNotFoundException | SQLException | NumberFormatException | HeadlessException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                               "Error", JOptionPane.ERROR_MESSAGE);
        }
        catch(NullPointerException ex)
        {
            JOptionPane.showMessageDialog(null, "Required field(s) Empty !!!", 
                        "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTable1AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jTable1AncestorAdded
    }//GEN-LAST:event_jTable1AncestorAdded

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
            float val;
            int qty;
            float Total = 0.0f;
            Connection con = con1.connection();
            for (int i = 0; i < jTable1.getRowCount(); i++) {
                System.out.println(jTable1.getRowCount());
                Object o = jTable1.getModel().getValueAt(i, 0);
                System.out.println(o.toString());
                if (o.toString().isEmpty()) {
                    break;
                } else {
                    String qry = "Select * From Items;";
                    Statement smt = con.createStatement();
                    ResultSet rs = smt.executeQuery(qry);
                    while (rs.next()) {
                        if (rs.getString("Order").equals(o.toString())) {
                            jTable1.getModel().setValueAt(rs.getString("Price"),
                                    i, 2);
                            val = Float.parseFloat(jTable1.getModel().getValueAt
                                    (i, 2).toString());
                            qty = Integer.parseInt(jTable1.getModel().getValueAt
                                    (i, 1).toString());
                            Total = (float) val * qty;

                        }
                    }
                    jTable1.getModel().setValueAt(Total, i, 3);
                    Price = Price + Total;
                    System.out.print("Price:" + Price);
                    PriceT.setText(Price + "");
                }
            }



        } catch (ClassNotFoundException | SQLException | NumberFormatException ex) {
           JOptionPane.showMessageDialog(null, 
                                "Request Not Complete !!!",
                                "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton3ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField CNoT;
    private javax.swing.JTextField PriceT;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
